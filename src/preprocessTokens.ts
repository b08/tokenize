import { Preprocessed } from "./preprocessed.type";
import { unique } from "@b08/array";

export function preprocessTokens(tokens: string[]): Preprocessed {
  const lengths = [];
  const keys = new Map<string, boolean>();
  tokens.forEach(function (token: string): void {
    keys.set(token, true);
    lengths.push(token.length);
  });

  lengths.sort();
  lengths.reverse();

  return {
    lengths: unique(lengths),
    keys
  };
}
