export interface Preprocessed {
    lengths: number[];
    keys: Map<string, boolean>;
}
