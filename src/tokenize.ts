import { Token } from "./token.type";
import { preprocessTokens } from "./preprocessTokens";
import { breakdown } from "./breakdown";

export function tokenize(source: string, tokens: string[]): Token[] {
  const preprocessed = preprocessTokens(tokens);
  return breakdown(source, preprocessed, (token, position) => ({ token, position }));
}

export function tokenizePlain(source: string, tokens: string[]): string[] {
  const preprocessed = preprocessTokens(tokens);
  return breakdown(source, preprocessed, token => token);
}
