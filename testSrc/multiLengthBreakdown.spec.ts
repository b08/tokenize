import { describe } from "@b08/test-runner";
import { breakdown } from "../src/breakdown";
import { Preprocessed } from "../src/preprocessed.type";

describe("breakdown", it => {
  it("should find long token", async expect => {
    // arrange

    const preprocessed: Preprocessed = {
      lengths: [5],
      keys: new Map<string, boolean>([["token", true]])
    };
    const source = "longtoken case";
    const expected = ["long", "token", " case"];

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect.deepEqual(result, expected);
  });

  it("should not break for token longer than input", async expect => {
    // arrange
    const map = new Map<string, boolean>();
    map.set("token", true);

    const preprocessed: Preprocessed = {
      lengths: [5],
      keys: map
    };

    const source = "tok";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect.deepEqual(result, [source]);
  });

  it("should find longer token over short", async expect => {
    // arrange
    const map = new Map<string, boolean>();
    map.set("tokens", true);
    map.set("token", true);

    const preprocessed: Preprocessed = {
      lengths: [6, 5],
      keys: map
    };

    const source = "tokens or tokeni";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect.deepEqual(result, ["tokens", " or ", "token", "i"]);
  });

  it("should find special symbols tokens (repeating test, but still should be)", async expect => {
    // arrange
    const map = new Map<string, boolean>();
    map.set("@@", true);
    map.set("@", true);

    const preprocessed: Preprocessed = {
      lengths: [2, 1],
      keys: map
    };

    const source = "@tokens@@ or tokeni@";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect.deepEqual(result, ["@", "tokens", "@@", " or tokeni", "@"]);
  });
});
