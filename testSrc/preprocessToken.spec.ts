import { preprocessTokens } from "../src/preprocessTokens";
import { describe } from "@b08/test-runner";

describe("preprocessTokens", it => {
    it("should return hashset containing all tokens", async expect => {
        // arrange
        const input = ["a", "as", "fe", "vervv", "asvrae"];

        // act
        const result = preprocessTokens(input);

        // assert
        input.forEach(token => expect.true(result.keys.has(token)));
    });

    it("should return lengths of tokens", async expect => {
        // arrange
        const input = ["a", "as", "fe", "vervv", "asvrae"];

        // act
        const result = preprocessTokens(input);

        // assert
        expect.deepEqual(result.lengths, [6, 5, 2, 1]);
    });
});
