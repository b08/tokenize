import { breakdown } from "../src/breakdown";
import { Preprocessed } from "../src/preprocessed.type";
import { describe } from "@b08/test-runner";

describe("breakdown", it => {
  it("should find single token", async expect => {
    // arrange
    const map = new Map<string, boolean>();
    map.set(" ", true);

    const preprocessed: Preprocessed = {
      lengths: [1],
      keys: map
    };

    const source = "simple case";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect.deepEqual(result, ["simple", " ", "case"]);
  });

  it("should find two short tokens", async expect => {
    // arrange
    const map = new Map<string, boolean>();
    map.set(" ", true);
    map.set("T", true);

    const preprocessed: Preprocessed = {
      lengths: [1],
      keys: map
    };

    const source = "twoToken case";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect.deepEqual(result, ["two", "T", "oken", " ", "case"]);
  });

  it("should return input if no token present", async expect => {
    // arrange
    const map = new Map<string, boolean>();
    map.set("w", true);

    const preprocessed: Preprocessed = {
      lengths: [1],
      keys: map
    };

    const source = "noToken case";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect.deepEqual(result, [source]);
  });
});
